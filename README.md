### README

# Este sistema consiste em:
 - Enviar e-mail
 - Registrar quem enviou o e-mail


# O banco de dados possui uma tabela chamada email com os seguintes atributos:
 - remetente: quem está enviando o e-mail
 - destinatario: para quem deve ser enviado o e-mail
 - assunto: assunto do e-mail
 - mensagem: a mensagem do e-mail

# Para clonar o projeto basta executar o seguinte comando:
- `git clone https://gitlab.com/marilia_ribeiro/enviar_email.git`


Para executar este sistema é necessário ter: Python PIP, Django e uma virtualenv instalados em seu computador. Caso você não tenha instalado em sua máquina siga os passos a seguir

# Instalando o Python PIP:
- `sudo apt-get install python-pip`

# Instalando a virtualenv e o virtualenvwrapper:
- `pip install virtualenv`
- `pip install virtualenvwrapper`

# Ativando o virtualenvwrapper
- `source virtualenvwrapper.sh`

# Criando uma virtualenv
- `mkvirtualenv artia`

# Ativando a virtualenv
- `workon artia`

### Para executar o projeto siga as seguintes instruções:
- Acesse a pasta do projeto que você acabou de clonar: `cd enviar_email`
- Com a virtualenv ativa instale as dependências do projeto: `pip install -r requirements.txt`
- Migre o banco de dados:
   - `python manage.py makemigrations`
   - `python manage.py migrate`
- Execute o script que popula o banco de dados: `python manage.py populate`
    - Para se autenticar no sistema você pode criar uma conta ou utilizar os usuários já cadastrados pelo populate. Existem três usuários cadastrados.
        - usuário: admin / senha: admin123
        - usuário: artia / senha: admin123
        - usuário: marilia / senha: admin123
- Execute o servidor django: `python manage.py runserver`
- Acesse a URL de autenticação no sistema: `http://localhost:8000/accounts/login/`

### Para enviar o e-mail é necessário colocar um e-mail do gmail válido. Para isso você pode adicionar seu e-mail no arquivo de configuração do projeto.

- Dentro da pasta do projeto, acesse o diretório `enviar_email`
- Abra o arquivo `settings.py`
- Adicione seu e-mail e senha nas linhas `114 e 115` desse arquivo
- Salve e envie e-mails utilizando o sistema.


### Para executar os casos de teste execute o seguinte comando: 
- `python manage.py test`
- Os testes incluem:
    - O cadastro de usuários
    - O cadastro de emails na tabela de e-mails
    - O envio de e-mail utilizando o send_email do django





