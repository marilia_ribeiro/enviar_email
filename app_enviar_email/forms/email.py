# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from app_enviar_email.models import Email


class EmailForm(forms.ModelForm):
    class Meta:
        model = Email
        exclude = ('remetente', 'data')
