# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import *

from app_enviar_email.forms.email import EmailForm
from app_enviar_email.models import Email


class EmailCreateView(CreateView):
    model = Email
    form_class = EmailForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.remetente = self.request.user
        self.object.save()

        enviar_email(self.object)

        return super(EmailCreateView, self).form_valid(form)


class EmailListView(ListView):
    queryset = Email.objects.all()

    def get_queryset(self, **kwargs):
        queryset = Email.objects.filter(remetente=self.request.user).order_by('-id').distinct()
        return queryset


def enviar_email(objeto):
    if objeto.assunto and objeto.mensagem and objeto.remetente:
        try:
            send_mail(objeto.assunto, objeto.mensagem, objeto.remetente, [objeto.destinatario])
        except BadHeaderError:
            return HttpResponse('Cabeçalho inválido.')
        return HttpResponseRedirect('/email/enviados/')
    else:
        return HttpResponse('Verifique se todos os campos foram preenchidos.')
