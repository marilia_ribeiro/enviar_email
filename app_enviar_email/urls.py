# coding=utf-8
from __future__ import unicode_literals

from django.conf.urls import url

from app_enviar_email.views import *

urlpatterns = [
    url(r'^enviar/$', EmailCreateView.as_view(), name='email-form'),
    url(r'^enviados/$', EmailListView.as_view(), name='email-list'),
]
