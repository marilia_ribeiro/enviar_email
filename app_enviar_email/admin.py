# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from app_enviar_email.models import Email

# Register your models here.

admin.site.register(Email)