# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


# Create your models here.
class Email(models.Model):
    remetente = models.ForeignKey(User, null=False, blank=True, related_name=u'rementente', verbose_name=u'Remetente', help_text=u'Deve conter o usuário remetente da mensagem')
    destinatario = models.EmailField(max_length=250, null=False, blank=False, verbose_name=u'Destinatário', help_text=u'Deve conter o e-mail de quem você deseja enviar a mensagem.')
    assunto = models.CharField(max_length=150, null=True, blank=True, default=u'Sem assunto', verbose_name=u'Assunto', help_text='Deve conter o assunto da mensagem.')
    mensagem = models.TextField(max_length=500, null=False, blank=False, verbose_name=u'Mensagem',
                                help_text='Deve conter uma mensagem.')
    data = models.DateTimeField(auto_now_add=True, verbose_name='Data', help_text='Deve conter a data e hora em que a mensagem foi enviada')

    def __unicode__(self):
        return u'%s' % self.assunto

    def get_absolute_url(self):
        return reverse('email-list')

    class Meta:
        app_label = 'app_enviar_email'
        verbose_name = 'Email'
        verbose_name_plural = 'Emails'
