# -*- coding: utf-8 -*-
from datetime import *

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from app_enviar_email.models import Email
from datetime import *

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from app_enviar_email.models import Email


class Command(BaseCommand):


    args = '<foo bar ...>'
help = 'our help string comes here'


def _populate(self):
    usuario_1, created = User.objects.get_or_create(
        username=u'admin',
        email=u'admin@admin.com',
        first_name=u'Admin',
        last_name=u'Admin'
    )
    usuario_1.set_password(u'admin123')
    usuario_1.save()

    usuario_2, created = User.objects.get_or_create(
        username=u'artia',
        email=u'artia@artia.com',
        first_name=u'Artia',
        last_name=u'Artia'
    )
    usuario_2.set_password(u'admin123')
    usuario_2.save()

    usuario_3, created = User.objects.get_or_create(
        username=u'marilia',
        email=u'marilia.ribeirods@gmail.com',
        first_name=u'Marilia',
        last_name=u'Ribeiro'
    )
    usuario_3.set_password(u'admin123')
    usuario_3.save()

    email_1, created = Email.objects.get_or_create(
        remetente=usuario_1,
        destinatario=u'marilia.ribeirods@gmail.com',
        assunto='Populate',
        mensagem='Populate',
        data=datetime.now()
    )
    email_1.save()

    email_2, created = Email.objects.get_or_create(
        remetente=usuario_2,
        destinatario=u'marilia.ribeirods@gmail.com',
        assunto='Populate',
        mensagem='Populate',
        data=datetime.now()
    )
    email_2.save()

    email_3, created = Email.objects.get_or_create(
        remetente=usuario_3,
        destinatario=u'marilia.ribeirods@gmail.com',
        assunto='Populate',
        mensagem='Populate',
        data=datetime.now()
    )
    email_3.save()


def handle(self, *args, **options):
    self._populate()
