# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.test import Client
from django.test import TestCase


class UserTests(TestCase):
    def setUp(self):
        self.client = Client()

    # Cadastro de usuários
    def user_add(self, username, email, password, first_name, last_name):
        usuario = User.objects.create(username=username, email=email, password=password, first_name=first_name,
                                      last_name=last_name)
        usuario.save()
        return usuario

    def test_user(self):
        user1 = self.user_add('admin', 'admin@admin.com', 'admin123', 'Admin', 'Admin')
        user2 = self.user_add('artia', 'artia@artia.com', 'admin123', 'Artia', 'Artia')
        user3 = self.user_add('marilia', 'marilia.ifc@gmail.com', 'admin123', 'Marilia', 'Ribeiro')
