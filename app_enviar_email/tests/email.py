# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from datetime import datetime

from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.test import Client
from django.test import TestCase

from app_enviar_email.models import Email


class EmailTests(TestCase):
    def setUp(self):
        self.client = Client()

    # cadastrar registros na tabela de email
    def email(self, remetente, destinatario, assunto, mensagem, data):
        email = Email(remetente=remetente, destinatario=destinatario, assunto=assunto, mensagem=mensagem, data=data)
        email.save()
        return email

    # enviar email
    def enviar_email(self, assunto, mensagem, remetente, destinatario):
        return send_mail(assunto, mensagem, remetente, [destinatario])

    # cadastro de usuários
    def user_add(self, username, email, password, first_name, last_name):
        usuario = User.objects.create(username=username, email=email, password=password, first_name=first_name,
                                      last_name=last_name)
        usuario.save()
        return usuario

    def testObjectCreate(self):
        # cadastro de usuários
        user = self.user_add('marilia', 'marilia.ifc@gmail.com', 'admin123', 'Marilia', 'Ribeiro')

        # cadastrar registros na tabela de email
        email = self.email(user, 'marilia.ifc@gmail.com', 'Test Case', 'Teste do Django', datetime.now())

        # enviar email
        self.assertEquals(self.enviar_email(email.assunto, email.mensagem, email.remetente, email.destinatario), 1)
